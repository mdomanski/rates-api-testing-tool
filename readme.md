# Rates API testing tool

Tool checks **Rates API** (https://ratesapi.io/) responses, status codes. **API documentation:** https://ratesapi.io/documentation/

## How to run:

Download project from github

You can run given tests via files *.feature (https://imgur.com/o2ABkMm)

You can run all tests by running class **CucumberTestRunner** (https://imgur.com/a/ADKhLQe)

## Others:

Project:

has implemented logger log4j

uses Junit, Cucumber and Rest Assured





