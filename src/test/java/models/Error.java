package models;

/**
 * Represents json error response from rates api
 * @author Michal
 */
public class Error {
    private String error;

    public String getError() {
        return error;
    }

}
