package models;

import java.util.Map;

/**
 * Represents correct json response from rates api
 * @author Michal
 */
public class RatesResponse {

    private String base;
    private Map<String, Float> rates;
    private String date;

    public String getBase() {
        return base;
    }

    public String getDate() {
        return date;
    }

    public Map<String, Float> getRates() {
        return rates;
    }
}
