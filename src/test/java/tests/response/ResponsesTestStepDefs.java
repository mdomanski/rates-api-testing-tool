package tests.response;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import models.RatesResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Set;

import static utils.ApiUtil.getRequestResponse;

/**
 * Class implements steps for Responses.feature
 * Methods checks responses body for requests
 * @author Michal
 */
public class ResponsesTestStepDefs {

    private static final Logger log = LogManager.getLogger(ResponsesTestStepDefs.class);
    private Response response;
    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private RatesResponse ratesResponse;
    private String EUR = "EUR";
    private String USD = "USD";
    private String PLN = "PLN";
    private String GBP = "GBP";

    @Given("Rates from API {string} to check response")
    public void ratesFromAPIToCheckResponse(String arg0) {
        response = getRequestResponse(arg0);
        log.debug("Response from API is ready");
    }

    @When("The API is available to check response")
    public void theAPIIsAvailableToCheckResponse() {
        Assert.assertNotNull(response);
        ratesResponse = response.as(RatesResponse.class);
        log.debug("API is available");
    }

    @Then("Check response is correct")
    public void checkResponseIsCorrect() {
        checkResponseCommons(EUR);
        checkSymbol(ratesResponse.getRates().keySet());
        log.debug("Response is checked");
    }

    @Then("Check response with base is correct")
    public void checkResponseWithBaseIsCorrect() {
        checkResponseCommons(USD);
        checkSymbol(ratesResponse.getRates().keySet());
        log.debug("Response of request with base is checked");
    }

    /**
     * Checks common asserts for response from api
     * @param base - parameter base
     */
    private void checkResponseCommons(String base) {
        Assert.assertNotNull("Rates object is null", ratesResponse);
        Assert.assertNotNull("Field 'base' is null", ratesResponse.getBase());
        Assert.assertEquals("Field 'base' isn't correct", base, ratesResponse.getBase());
        Assert.assertNotNull("Field 'rates' is null", ratesResponse.getRates());
        Assert.assertFalse("Field 'rates' is empty", ratesResponse.getRates().isEmpty());
        Assert.assertNotNull("Field 'date' is empty", ratesResponse.getDate());
    }

    /**
     * Checks first symbol from api
     * @param apiSymbols - set with all symbols from api
     */
    private void checkSymbol(Set<String> apiSymbols) {
        Assert.assertEquals("Wrong rate symbol", GBP, apiSymbols.stream().findFirst().get());
    }

    /**
     * Checks response, incl. common asserts and symbols
     * @param base - base parameter
     * @param symbols - local static symbols
     */
    private void checkResponseWithParams(String base, String[] symbols) {
        checkResponseCommons(base);
        Assert.assertArrayEquals("Wrong rate symbols", symbols, ratesResponse.getRates().keySet().toArray());
    }

    private void checkDate(String date) {
        Assert.assertEquals("Wrong date value", date, ratesResponse.getDate());
    }

    @Then("Check response with symbol is correct")
    public void checkResponseWithSymbolIsCorrect() {
        checkResponseWithParams(EUR, new String[]{USD});
        log.debug("Response of request with symbol is checked");
    }

    @Then("Check response with symbols is correct")
    public void checkResponseWithSymbolsIsCorrect() {
        checkResponseWithParams(EUR, new String[]{USD, GBP});
        log.debug("Response of request with symbols is checked");
    }

    @Then("Check response with symbols and base is correct")
    public void checkResponseWithSymbolsAndBaseIsCorrect() {
        checkResponseWithParams(PLN, new String[]{USD, GBP});
        log.debug("Response of request with symbols and base is checked");
    }

    @Then("Check response with symbols same and base is correct")
    public void checkResponseWithSymbolsSameAndBaseIsCorrect() {
        checkResponseWithParams(PLN, new String[]{USD});
        log.debug("Response of request with same symbols and base is checked");
    }

    @Then("Check response with specific date {string} is correct")
    public void checkResponseWithSpecificDateIsCorrect(String date) {
        checkResponseCommons(EUR);
        checkDate(date);
        log.debug("Response of request with specific date is checked");
    }

    @Then("Check response with future date is correct")
    public void checkResponseWithFutureDateIsCorrect() {
        checkResponseCommons(EUR);
        checkDate(LocalDate.now().format(formatter));
        log.debug("Response of request with future date is checked");
    }

    @Then("Check response with specific date and base {string} is correct")
    public void checkResponseWithSpecificDateAndBaseIsCorrect(String date) {
        checkResponseCommons(PLN);
        checkDate(date);
        log.debug("Response of request with date and base is checked");
    }

    @Then("Check response with specific date and symbols {string} is correct")
    public void checkResponseWithSpecificDateAndSymbolsIsCorrect(String date) {
        checkResponseWithParams(EUR, new String[]{USD, GBP});
        checkDate(date);
        log.debug("Response of request with date and symbols is checked");
    }

    @Then("Check response with specific date, symbols, base {string} is correct")
    public void checkResponseWithSpecificDateSymbolsBaseIsCorrect(String date) {
        checkResponseWithParams(PLN, new String[]{USD, GBP});
        checkDate(date);
        log.debug("Response of request with date, base and symbols is checked");
    }
}
