Feature: Responses feature

  Scenario: Check response of request /latest
    Given Rates from API "/latest" to check response
    When The API is available to check response
    Then Check response is correct

  Scenario: Check response of request /latest with base
    Given Rates from API "/latest?base=USD" to check response
    When The API is available to check response
    Then Check response with base is correct

  Scenario: Check response of request /latest with symbol
    Given Rates from API "/latest?symbols=USD" to check response
    When The API is available to check response
    Then Check response with symbol is correct

  Scenario: Check response of request /latest with symbols
    Given Rates from API "/latest?symbols=USD,GBP" to check response
    When The API is available to check response
    Then Check response with symbols is correct

  Scenario: Check response of request /latest with symbols and base
    Given Rates from API "/latest?base=PLN&symbols=USD,GBP" to check response
    When The API is available to check response
    Then Check response with symbols and base is correct

  Scenario: Check response of request /latest with same symbols and base
    Given Rates from API "/latest?base=PLN&symbols=USD,USD" to check response
    When The API is available to check response
    Then Check response with symbols same and base is correct

  Scenario: Check response of request with past date
    Given Rates from API "/2020-06-04" to check response
    When The API is available to check response
    Then Check response with specific date "2020-06-04" is correct

  Scenario: Check response of request with past date
    Given Rates from API "/2010-06-04" to check response
    When The API is available to check response
    Then Check response with specific date "2010-06-04" is correct

  Scenario: Check response of request with date and base
    Given Rates from API "/2020-06-04?base=PLN" to check response
    When The API is available to check response
    Then Check response with specific date and base "2020-06-04" is correct

  Scenario: Check response of request with date and symbols
    Given Rates from API "/2020-06-04?symbols=USD,GBP" to check response
    When The API is available to check response
    Then Check response with specific date and symbols "2020-06-04" is correct

  Scenario: Check response of request with date, symbols, base
    Given Rates from API "/2020-06-04?base=PLN&symbols=USD,GBP" to check response
    When The API is available to check response
    Then Check response with specific date, symbols, base "2020-06-04" is correct

  Scenario: Check response of request with future date
    Given Rates from API "/2021-06-04" to check response
    When The API is available to check response
    Then Check response with future date is correct
