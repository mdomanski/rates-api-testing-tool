package tests.status;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import models.RatesResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import static utils.ApiUtil.*;

/**
 * Class implements steps for Statuses.feature
 * Methods checks http status codes for requests
 *
 * @author Michal
 */
public class StatusTestStepDefs {

    private static final Logger log = LogManager.getLogger(StatusTestStepDefs.class);
    private List<String> symbols;
    private String params = "";
    private int statusCode;
    private Response response;

    @Given("Rates for specific date {string} from API to check status")
    public void ratesForSpecificDateFromAPIToCheckStatus(String date) {
        response = getRequestResponse("/" + date);
    }

    @Given("Rates from API to check status")
    public void ratesFromAPIToCheckStatus() {
        response = getRequestResponse("/latest");
        log.debug("Response from API is ready");
    }

    @When("The API is available to check status")
    public void theAPIIsAvailableToCheckStatus() {
        Assert.assertNotNull(response);
        statusCode = getStatusCode(response);
        log.debug("API is available");
    }

    @Then("Check status code is {int}")
    public void checkStatusCodeIs(int arg0) {
        Assert.assertEquals(arg0, statusCode);
        log.debug("Status code: " + arg0 + " is correct");
    }

    @Then("Get currency symbols")
    public void getCurrencySymbols() {
        symbols = new ArrayList<>(response.as(RatesResponse.class).getRates().keySet());
        log.debug("Currency symbols ready");
    }

    @Then("Check symbols list is not empty")
    public void checkSymbolsListIsNotEmpty() {
        Assert.assertTrue(symbols != null && !symbols.isEmpty());
        log.debug("Currency symbols list is not empty");
    }

    @Then("Check status code is {int} for each request with symbols")
    public void checkStatusCodeIsForEachRequestWithSymbols(int status) {

        /* This code block builds parameter 'symbols' with multiple for each iteration
        Example:
        1 - /latest?symbols=USD
        2 - /latest?symbols=USD,GBP
        3 - /latest?symbols=USD,GBP,EUR
        */

        IntStream.range(0, symbols.size()).forEach(i -> {
            if (i == 0) {
                params = params + symbols.get(i);
            } else {
                params = params + "," + symbols.get(i);
            }
            testStatusCode(status, "/latest?symbols=" + params);
        });
        log.debug("Status codes for each request with symbols are OK");
    }

    @Then("Check status code is {int} for each request with symbols and base")
    public void checkStatusCodeIsForEachRequestWithSymbolsAndBase(int status) {
        List<String> bases = symbols;
        /* Builds url for each various symbol and various base
         * Example:
         * /latest?symbols=USD&base=GBP, /latest?symbols=PLN&base=GBP, /latest?symbols=GBP&base=PLN
         */

        symbols.forEach(s -> bases.forEach(b -> testStatusCode(status, "/latest?symbols=" + s + "&base=" + b)));
        log.debug("Status codes for each request with symbols and base are OK");
    }

    @Then("Check status code is {int} for each request with base")
    public void checkStatusCodeIsForEachRequestWithBase(int status) {
        symbols.forEach(symbol -> testStatusCode(status, "/latest?base=" + symbol));
        log.debug("Status codes for each request with base are OK");
    }

    private void testStatusCode(int code, String url) {
        response = getRequestResponse(url);
        checkStatusCode(code, getStatusCode(response));
    }
}
