Feature: Statuses feature

  Scenario: Check status of request /latest
    Given Rates from API to check status
    When The API is available to check status
    Then Check status code is 200

  Scenario: Check status of request /latest with different base parameter
    Given Rates from API to check status
    When The API is available to check status
    Then Get currency symbols
    Then Check symbols list is not empty
    Then Check status code is 200 for each request with base

  Scenario: Check status of request /latest with symbols
    Given Rates from API to check status
    When The API is available to check status
    Then Get currency symbols
    Then Check symbols list is not empty
    Then Check status code is 200 for each request with symbols

  Scenario: Check status of request /latest with symbols and base
    Given Rates from API to check status
    When The API is available to check status
    Then Get currency symbols
    Then Check symbols list is not empty
    Then Check status code is 200 for each request with symbols and base

  Scenario: Check status of request for specific date
    Given Rates for specific date "2020-05-05" from API to check status
    When The API is available to check status
    Then Check status code is 200

  Scenario: Check status of request for specific future date
    Given Rates for specific date "2021-05-05" from API to check status
    When The API is available to check status
    Then Check status code is 200

  Scenario: Check status of request for specific future date
    Given Rates for specific date "2021-05-05" from API to check status
    When The API is available to check status
    Then Check status code is 200

  Scenario: Check status of request for specific wrong date
    Given Rates for specific date "2021-05-xx" from API to check status
    When The API is available to check status
    Then Check status code is 400

  Scenario: Check status of request for specific wrong format date
    Given Rates for specific date "05-05-2021" from API to check status
    When The API is available to check status
    Then Check status code is 400