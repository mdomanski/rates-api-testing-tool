package tests.date;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import utils.date.DateChecker;

/**
 * Unit tests for date checker
 * @author Michal
 */
public class DateCheckerTest {

    private DateChecker dateChecker;

    @Before
    public void init() {
        dateChecker = new DateChecker();
    }

    @Test
    public void testValidDateAndFormat() {
        Assert.assertTrue(dateChecker.isCorrectDate( "2020-06-05", "yyyy-MM-dd"));
    }

    @Test
    public void testInvalidFormat() {
        Assert.assertFalse(dateChecker.isCorrectDate( "20-06-2005", "yyyy-MM-dd"));
    }

    @Test
    public void testInvalidDate() {
        Assert.assertFalse(dateChecker.isCorrectDate( "2020-06-305", "yyyy-MM-dd"));
    }

    @Test
    public void testInvalidDay() {
        Assert.assertFalse(dateChecker.isCorrectDate( "2020-06-35", "yyyy-MM-dd"));
    }

    @Test
    public void testInvalidMonth() {
        Assert.assertFalse(dateChecker.isCorrectDate( "2020-13-25", "yyyy-MM-dd"));
    }

    @Test
    public void testInvalidYear() {
        Assert.assertFalse(dateChecker.isCorrectDate( "21020-06-35", "yyyy-MM-dd"));
    }

    @Test
    public void test29Feb2020() {
        Assert.assertTrue(dateChecker.isCorrectDate( "2020-02-29", "yyyy-MM-dd"));
    }

    @Test
    public void test29Feb2019() {
        Assert.assertFalse(dateChecker.isCorrectDate( "2019-02-29", "yyyy-MM-dd"));
    }
}
