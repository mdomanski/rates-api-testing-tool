package tests.errors;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import models.Error;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;

import static utils.ApiUtil.*;
import static utils.ApiUtil.getRequestResponse;

/**
 * Class implements steps for Errors.feature
 * Methods checks errors for incomplete, incorrect urls
 * @author Michal
 */
public class ErrorsTestStepDefs {

    private static final Logger log = LogManager.getLogger(ErrorsTestStepDefs.class);
    private Response response;
    private Error error;

    @Given("Rates from API {string} to check error")
    public void ratesFromAPIToCheckError(String arg0) {
        response = getRequestResponse(arg0);
        log.debug("Response from API is ready");
    }

    @When("The API is available to check error")
    public void theAPIIsAvailableToCheckError() {
        Assert.assertNotNull(response);
        log.debug("API is available");
    }

    @Then("Check response and status {int} of incomplete url")
    public void checkResponseAndStatusOfIncompleteUrl(int arg0) {
        checkResponse(arg0, "api");
        log.debug("Checked response for incomplete url");
    }

    @Then("Check response and status {int} of incorrect url")
    public void checkResponseAndStatusOfIncorrectUrl(int arg0) {
        checkResponse(arg0, "test");
        log.debug("Checked response for incorrect url");
    }

    /**
     * Checks if response contains given message and status code (400, 404)
     * @param statusCode - given status code
     * @param message - special word in error message
     */
    private void checkResponse(int statusCode, String message) {
        error = response.as(Error.class);
        String fullMessage = "time data '" + message + "' does not match format '%Y-%m-%d'";
        Assert.assertEquals("Wrong error message", fullMessage, error.getError());
        checkStatusCode(statusCode, getStatusCode(response));
    }

    @Then("Check status {int}")
    public void checkStatus(int arg0) {
        checkStatusCode(arg0, getStatusCode(response));
        log.debug("Status code: " + arg0 + " is correct");
    }
}
