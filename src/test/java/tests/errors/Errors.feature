Feature: Errors feature

  Scenario: Check response of incomplete url
    Given Rates from API " " to check error
    When The API is available to check error
    Then Check response and status 400 of incomplete url

  Scenario: Check response of incorrect url
    Given Rates from API "/test" to check error
    When The API is available to check error
    Then Check response and status 400 of incorrect url

  Scenario: Check error not found
    Given Rates from API "/latest/888" to check error
    When The API is available to check error
    Then Check status 404