package tests;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/**
 * Main runner class for all cucumber scenarios. When we run this class, all tests will be executed
 * @author Michal
 */

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty"}, features = {"src/test/java/tests"})
public class CucumberTestRunner {
}
