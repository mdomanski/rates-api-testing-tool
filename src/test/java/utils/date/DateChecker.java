package utils.date;

import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DateChecker {

    /**
     * Checks is correct given date for given format
     * @param dateText - date
     * @param format - date format
     * @return date state
     */
    public boolean isCorrectDate(String dateText, String format) {

        if (dateText == null || format == null) {
            return false;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(format);
        sdf.setLenient(false);

        try {
            sdf.parse(dateText);

        } catch (ParseException e) {
            return false;
        }
        return true;
    }
}
