package utils;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Assert;

/**
 * Additional methods used in project
 * @author Michal
 */
public class ApiUtil {

    /**
     * Gets status code from RestAssured's object Response
     * @param response - response from request
     * @return value of status code (e.g. 200, 400, 404, etc)
     */
    public static int getStatusCode(Response response) {
        return response.getStatusCode();
    }

    /**
     * Gets response from api
     * @param uri - part of url
     * @return object Response
     */
    public static Response getRequestResponse(String uri) {
        return RestAssured.get(Constants.BASE_URL + uri);
    }

    public static void checkStatusCode(int expected, int current) {
        Assert.assertEquals("Wrong status code", expected, current);
    }
}
